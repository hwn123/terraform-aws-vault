# Vault

This module is used to install vault in kubernetes namespace

# Pre-requirements

Kubernetes namespace

SQL database configured with appropriate schema

SQL database connection string with credentials

KMS key and aws account to access it