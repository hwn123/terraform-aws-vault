##############################################################################
# Create K8s secret to inject into Vault pods allowing the ability to pull..
# .. and decrypt KMS keys in order to unseal itself
##############################################################################

resource "kubernetes_secret" "aws" {
  metadata {
    name      = "vault-aws-auth"
    namespace = var.k8s_namespace
    labels = {
      "app.kubernetes.io/name"       = "vault-aws-secret",
      "app.kubernetes.io/managed-by" = "Terraform",
      "pp.jagex.com/context"         = "iac",
    }
  }
  data = {
    AWS_ACCESS_KEY_ID        = var.aws_config["vault_aws_key_id"]
    AWS_SECRET_ACCESS_KEY    = var.aws_config["vault_aws_secret"]
    VAULT_AWSKMS_SEAL_KEY_ID = var.aws_config["vault_unseal_key_id"]
  }
}
