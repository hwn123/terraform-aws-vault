
variable "k8s_namespace" {
  type        = string
  description = "Namespace in kubernetes"
}

variable "vault_version" {
  type        = string
  description = "Vault version"
}

variable "fqdn" {
  type        = string
  description = "FQDN to use with vault"
}

##########################################
# PostgreSQL
##########################################

variable "postgresql_config" {
  type        = map(string)
  description = "Map of pgsql config"
}

##########################################
# AWS
##########################################

variable "aws_config" {
  type        = map(string)
  description = "Map of AWS config"
}


variable "kubernetes_tags" {
  type        = map(string)
  description = "Map of tags to add to K8s resources"
  default     = {}
}

variable "public_subnets" {
  type        = list(string)
  description = "List of Public subnet IDs for the ALBs"
}

variable "security_groups" {
  type        = list(string)
  description = "List of allowed security groups for ingress"
}