
resource "aws_kms_key" "seal" {
  description             = "This key is used to encrypt RDS"
  deletion_window_in_days = 30
  enable_key_rotation     = true
}

resource "aws_kms_alias" "database" {
  name          = "alias/vault-seal-fqdn"
  target_key_id = aws_kms_key.seal.key_id
}
