resource "helm_release" "vault" {
  name          = "vault"
  namespace     = var.k8s_namespace
  repository    = "https://helm.releases.hashicorp.com"
  chart         = "vault"
  vault_version = var.vault_version

  values = [
    templatefile("${path.module}/templates/values.yaml.tmpl", {
      postgresql_endpoint = var.postgresql_config["db_endpoint"],
      postgresql_db       = var.postgresql_config["db_name"],
      pg_user             = var.postgresql_config["admin_user"],
    })
  ]

  set_sensitive {
    name  = "pg_password"
    value = var.postgresql_config["admin_password"]
  }
}
